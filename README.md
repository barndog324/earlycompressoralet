# Early Compressor Alert

This console application searches all configured jobs for a specific list of compressors that need to be ordered in advance. Currently runs every day at 10am. The program produces a spreadsheet and then emails it to a list of hard-coded email addresses.

## Project Structure

### EarlyCompressorAlert
This project is a .NET Framework 4.5 console application project.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures, as well as a Visual Studio dataset. The following stored procedures are used:

* EarlyAlert_GetOA_OrderList
* EarlyAlert_GetOA_CompConfig
* EarlyAlert_GetOA_CompressorsRev5
* EarlyAlert_GetOA_CompressorsRev6
* EarlyAlert_GetTandemCompressors
* EarlyAlert_GetOA_LaborDetails

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection string in App.configto point to the appropriate local or dev KCC database. Build the solution and run it. 

## How to deploy
In Visual Studio, select the appropriate build configuration. Open the project properties and in Build tab set the output path to the directory to be deployed to.

## Author/Devs
Tony Thoman