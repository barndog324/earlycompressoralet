﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarlyCompressorAlert
{
    class MainDTO
    {
        public string AuxBox { get; set; }
        public string Comp1PartNum { get; set; }
        public string Comp2PartNum { get; set; }
        public string Comp3PartNum { get; set; }
        public string Comp4PartNum { get; set; }
        public string Comp5PartNum { get; set; }
        public string Comp6PartNum { get; set; }
        public string Comp1Qty { get; set; }
        public string Comp2Qty { get; set; }
        public string Comp3Qty { get; set; }
        public string Comp4Qty { get; set; }
        public string Comp5Qty { get; set; }
        public string Comp6Qty { get; set; }
        public string CurrentStatus { get; set; }
        public string Description { get; set; }
        public string Hertz { get; set; }
        public string JobNum { get; set; }
        public string ModelNo { get; set; }
        public string ModelRev { get; set; }
        public string OrderNum { get; set; }
        public string OrderLine { get; set; }
        public string OrdeRelNum { get; set; }
        public string PartNum { get; set; }
        public string Phase { get; set; }
        public string ProdLine { get; set; }
        public string RequiredQty { get; set; }
        public string StartDate { get; set; }
        
        public string UOM { get; set; }
        public string UnitType { get; set; }
        public string Voltage { get; set; }


    }
}
