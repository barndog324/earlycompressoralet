﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace EarlyCompressorAlert
{
    class Program
    {
       
        static void Main(string[] args)
        {          

            Console.WriteLine(DateTime.Now);
            processOA_UnitList();
            Console.WriteLine(DateTime.Now);            
        }        
     
        static private void processOA_UnitList()
        {
            string modelNo = "";            
            string partNum = "";
            string qtyStr = "";
            string rlaStr = "";
            
            bool configFound = false;            

            MainBO objMain = new MainBO();
            DataTable dtOrders = objMain.GetOA_OrderList();

            //using (StreamWriter sw = new StreamWriter(@"C:\Users\tonyt\Documents\EarlyAlertCompPartNums.csv"))            
            //using (StreamWriter sw = new StreamWriter(@"\\EPICOR905APP\Apps\OAU\EarlyAlertCompressor\EarlyAlertCompPartNums.csv"))
            using (StreamWriter sw = new StreamWriter(@"\\KCCWVPEPIC9APP\Apps\OAU\EarlyAlertCompressor\EarlyAlertCompPartNums.csv"))
            {
                sw.WriteLine("JobNum," + "StartDate," + "Comp1PartNum," + "Comp1Qty," + "Comp2PartNum," + "Comp2Qty," + "Comp3PartNum," + "Comp3Qty," + "Comp4PartNum," + "Comp4Qty," + "Comp5PartNum," + "Comp5Qty," + "Comp6PartNum," + "Comp6Qty,");

                foreach (DataRow row in dtOrders.Rows)
                {
                    objMain.JobNum = "";
                    objMain.PartNum = "";
                    objMain.OrderNum = "";
                    objMain.OrderLine = "";
                    objMain.OrdeRelNum = "";
                    objMain.Comp1PartNum = "";
                    objMain.Comp1Qty = "";
                    objMain.Comp2PartNum = "";
                    objMain.Comp2Qty = "";
                    objMain.Comp3PartNum = "";
                    objMain.Comp3Qty = "";
                    objMain.Comp4PartNum = "";
                    objMain.Comp4Qty = "";
                    objMain.Comp5PartNum = "";
                    objMain.Comp5Qty = "";
                    objMain.Comp6PartNum = "";
                    objMain.Comp6Qty = "";

                    objMain.JobNum = row["OrderNum"].ToString() + "-" + row["OrderLine"].ToString() + "-" + row["OrderRelNum"].ToString();
                    modelNo = row["ModelNo"].ToString();
                    objMain.ModelNo = modelNo;
                    objMain.StartDate = row["StartDate"].ToString();                   

                    if (objMain.ModelNo.Length == 39 || objMain.ModelNo.Length == 69)
                    {
                        objMain.ModelNo = modelNo.Substring(0, 7) + modelNo.Substring(12, 1);
                        configFound = objMain.GetOA_CompConfig();
                        if (configFound == false)
                        {
                            objMain.ModelNo = modelNo.Substring(0, 3) + "*" + modelNo.Substring(4, 3) + modelNo.Substring(12, 1);
                            configFound = objMain.GetOA_CompConfig();
                        }

                        if (configFound == true)
                        {
                            objMain.ModelNo = modelNo;
                            if (objMain.ModelNo.Length == 39 )
                            {
                                objMain.ModelRev = "Rev5";
                                switch (objMain.ModelNo.Substring(8, 1))
                                {
                                    case "1":
                                        objMain.Voltage = "115";                                       
                                        objMain.Phase = "1";
                                        objMain.Hertz = "60";
                                        break;
                                    case "2":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "1";
                                        objMain.Hertz = "60";
                                        break;
                                    case "3":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "4":
                                        objMain.Voltage = "460";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "5":
                                        objMain.Voltage = "575";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    default:
                                        objMain.Voltage = "208";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                }
                            }
                            else
                            {
                                objMain.ModelRev = "Rev6";
                                switch (modelNo.Substring(8, 1))
                                {
                                    case "1":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "2":
                                        objMain.Voltage = "230";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "3":
                                        objMain.Voltage = "460";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "4":
                                        objMain.Voltage = "575";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                    case "5":
                                        objMain.Voltage = "115";
                                        objMain.Phase = "1";
                                        objMain.Hertz = "60";
                                        break;
                                    case "6":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "1";
                                        objMain.Hertz = "60";
                                        break;
                                    case "7":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "50";
                                        break;
                                    case "8":
                                        objMain.Voltage = "380";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "50";
                                        break;
                                    case "9":
                                        objMain.Voltage = "208";
                                        objMain.Phase = "1";
                                        objMain.Hertz = "50";
                                        break;
                                    default:
                                        objMain.Voltage = "208";
                                        objMain.Phase = "3";
                                        objMain.Hertz = "60";
                                        break;
                                }
                            }
                            objMain.ModelNo = modelNo;
                            DataTable dtComp =  objMain.GetOA_Compressors();

                            int compNum = 1;

                            foreach(DataRow drComp in dtComp.Rows)
                            {
                                partNum = drComp["PartNum"].ToString();
                                qtyStr = drComp["ReqQty"].ToString();
                                rlaStr = drComp["RLA"].ToString();                                

                                if (partNum.Contains("RFGASM"))
                                {
                                    objMain.PartNum = partNum;
                                    DataTable dtPart = objMain.GetTandemCompAssemblyMtls();

                                    foreach (DataRow drPart in dtPart.Rows)
                                    {                                       
                                        objMain.PartNum = drPart["PartNum"].ToString();                                                                                
                                        DataTable dtCmpDtl = objMain.GetCompDetailData();
                                        
                                        if (dtCmpDtl.Rows.Count > 0)
                                        {
                                            if (compNum == 1)
                                            {
                                                objMain.Comp1PartNum = objMain.PartNum;
                                                objMain.Comp1Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                            else if (compNum == 2)
                                            {
                                                objMain.Comp2PartNum = objMain.PartNum;
                                                objMain.Comp2Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                            else if (compNum == 3)
                                            {
                                                objMain.Comp3PartNum = objMain.PartNum;
                                                objMain.Comp3Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                            else if (compNum == 4)
                                            {
                                                objMain.Comp4PartNum = objMain.PartNum;
                                                objMain.Comp4Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                            else if (compNum == 5)
                                            {
                                                objMain.Comp5PartNum = objMain.PartNum;
                                                objMain.Comp5Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                            else 
                                            {
                                                objMain.Comp6PartNum = objMain.PartNum;
                                                objMain.Comp6Qty = drPart["ReqQty"].ToString();
                                                ++compNum;
                                            }
                                        }
                                    }
                                }
                                else
                                {                                    
                                    if (rlaStr != "-1")
                                    {
                                        if (compNum == 1)
                                        {
                                            objMain.Comp1PartNum = objMain.PartNum;
                                            objMain.Comp1Qty = qtyStr;
                                            ++compNum;
                                        }
                                        else if (compNum == 2)
                                        {
                                            objMain.Comp2PartNum = objMain.PartNum;
                                            objMain.Comp2Qty = qtyStr;
                                            ++compNum;
                                        }
                                        else if (compNum == 3)
                                        {
                                            objMain.Comp3PartNum = objMain.PartNum;
                                            objMain.Comp3Qty = qtyStr;
                                            ++compNum;
                                        }
                                        else if (compNum == 4)
                                        {
                                            objMain.Comp4PartNum = objMain.PartNum;
                                            objMain.Comp4Qty = qtyStr;
                                            ++compNum;
                                        }
                                        else if (compNum == 5)
                                        {
                                            objMain.Comp5PartNum = objMain.PartNum;
                                            objMain.Comp5Qty = qtyStr;
                                            ++compNum;
                                        }
                                        else
                                        {
                                            objMain.Comp6PartNum = objMain.PartNum;
                                            objMain.Comp6Qty = qtyStr;
                                            ++compNum;
                                        }
                                    }
                                }
                            }

                            DataTable dtLabor = objMain.GetOA_LaborDetails();

                            if (dtLabor.Rows.Count == 0)
                            {
                                sw.WriteLine(objMain.JobNum + "," + objMain.StartDate + "," + objMain.Comp1PartNum + "," + objMain.Comp1Qty + "," + objMain.Comp2PartNum + "," +
                                             objMain.Comp2Qty + "," + objMain.Comp3PartNum + "," + objMain.Comp3Qty + "," + objMain.Comp4PartNum + "," + objMain.Comp4Qty + "," +
                                             objMain.Comp5PartNum + "," + objMain.Comp5Qty + "," + objMain.Comp6PartNum + "," + objMain.Comp6Qty);
                            }
                        }

                    }

                }
            }
                      
            MailMessage mail = new MailMessage();
            mail.To.Add("lking@KCCMfg.com");
            mail.To.Add("lwolz@KCCMfg.com");
            mail.To.Add("mpaulin@KCCMfg.com");
            mail.To.Add("dgoodman@KCCMfg.com");
            mail.To.Add("kevans@KCCMfg.com");
            mail.To.Add("tthoman@KCCMfg.com");

            mail.Subject = "Long Leadtime Compressor Alert";
            
            mail.Body = "See Attached";

            System.Net.Mail.Attachment attachment;
            
            //attachment = new System.Net.Mail.Attachment(@"\\EPICOR905APP\Apps\OAU\EarlyAlertCompressor\EarlyAlertCompPartNums.csv");
            attachment = new System.Net.Mail.Attachment(@"\\KCCWVPEPIC9APP\Apps\OAU\EarlyAlertCompressor\EarlyAlertCompPartNums.csv");

            mail.Attachments.Add(attachment);
            //mail.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient();
            //smtp.Host = "smtp.gmail.com";
            //smtp.Port = 587;

            //smtp.Credentials = new NetworkCredential("username@domain.com", "password");
            //smtp.EnableSsl = true;

            smtp.Send(mail);

        }

        
    }
}
