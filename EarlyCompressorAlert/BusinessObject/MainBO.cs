﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EarlyCompressorAlert
{
    class MainBO : MainDTO
    {
        public DataTable GetOA_OrderList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetOA_OrderList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public bool GetOA_CompConfig()
        {
            bool configFound = false;

            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetOA_CompConfig", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    configFound = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return configFound;
        }

        public DataTable GetOA_Compressors()
        {            
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                if (ModelRev == "Rev5")
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetOA_CompressorsRev5", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                            cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));                           
                            cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);                                   
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetOA_CompressorsRev6", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ModelNo", ModelNo);
                            cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                            cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public DataTable GetTandemCompAssemblyMtls()
        {           
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetTandemCompressors", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                              
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompDetailData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetCompressorDetailData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@Voltage", Int32.Parse(Voltage));
                        cmd.Parameters.AddWithValue("@Phase", Int32.Parse(Phase));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }        

        public DataTable GetOA_LaborDetails()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.EarlyAlert_GetOA_LaborDetails", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }        
    }
}
